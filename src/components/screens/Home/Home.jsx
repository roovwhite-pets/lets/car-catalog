import Cards from '@ui/Cards/Cards.jsx';
import Auth from '@ui/Auth/Auth.jsx';

const Home = () => {
  return (
    <>
      <Auth />

      <div className="container">
        <h1 className="center">Car Catalog</h1>
      </div>

      <Cards />
    </>
  );
};

export default Home;
