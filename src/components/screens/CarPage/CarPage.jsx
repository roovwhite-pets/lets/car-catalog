import { useEffect, useState } from 'react';
import { CarService } from '@services/car.service.js';
import { Link, useParams } from 'react-router-dom';
import Preloader from '@ui/Preloader/Preloader.jsx';
import styles from './CarPage.module.scss';

const CarPage = () => {
  const { id } = useParams();
  const [carData, setCarData] = useState({});
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (!id) return;

    const fetchCarData = async () => {
      const carData = await CarService.getCarById(id);

      setTimeout(() => {
        setLoading(true);
        setCarData(carData);
      }, 1000);
    };

    fetchCarData().catch(err => console.log('err', err));
  }, []);

  if (!carData.name && !loading) return <Preloader />;

  return (
    <>
      <div className="container">
        <div className={styles.wrapper}>
          <h1 className="center">{carData.name}</h1>
          <img className={styles.img} src={carData.img} alt={carData.name} />
          <div className={styles.price}>
            {Intl.NumberFormat('en-US', {
              style: 'currency',
              currency: 'USD',
            }).format(carData.price)}
          </div>
          <div className={styles.info}>{carData.info}</div>
          <Link className="button" to="/">
            Back
          </Link>
        </div>
      </div>
    </>
  );
};

export default CarPage;
