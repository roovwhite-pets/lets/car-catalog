import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Home from '@screens/Home/Home.jsx';
import CarPage from '@screens/CarPage/CarPage.jsx';
import NotFound from '@ui/Errors/404.jsx';

const PageRouter = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route element={<Home />} path="/" />
        <Route element={<CarPage />} path="/car/:id" />
        <Route element={<NotFound />} path="*" />
      </Routes>
    </BrowserRouter>
  );
};

export default PageRouter;
