import { useForm } from 'react-hook-form';
import styles from './CreateCardForm.module.scss';

const CreateCardForm = ({ setCars }) => {
  const {
    register,
    reset,
    handleSubmit,
    formState: { errors },
  } = useForm({
    mode: 'onChange',
  });

  const createCard = data => {
    setCars(prev => [
      {
        ...data,
        id: prev.length + 1,
        more: prev.length + 1,
      },
      ...prev,
    ]);

    reset();
  };

  return (
    <form className={styles.form} onSubmit={handleSubmit(createCard)}>
      <div className={styles.field}>
        <input
          className={styles.input}
          type="text"
          {...register('name', { required: 'Name is required!' })}
          placeholder="Name"
          aria-invalid={!!errors.name}
        />
        {errors?.name?.message && (
          <p className={styles.error}>{errors?.name?.message}</p>
        )}
      </div>
      <div className={styles.field}>
        <input
          className={styles.input}
          type="text"
          {...register('price', { required: 'Price is required!' })}
          placeholder="Price"
          aria-invalid={!!errors.name}
        />
        {errors?.price?.message && (
          <p className={styles.error}>{errors?.price?.message}</p>
        )}
      </div>
      <div className={styles.field}>
        <input
          className={styles.input}
          type="text"
          {...register('img', { required: 'Image url is required!' })}
          placeholder="Image Url"
          aria-invalid={!!errors.name}
        />
        {errors?.img?.message && (
          <p className={styles.error}>{errors?.img?.message}</p>
        )}
      </div>
      <div className={styles.field}>
        <button type="submit">Create</button>
      </div>
    </form>
  );
};

export default CreateCardForm;
