import { useContext } from 'react';
import { AuthContext } from '@/providers/AuthContext.jsx';
import styles from './Auth.module.scss';

const Auth = () => {
  const { user, setUser } = useContext(AuthContext);

  return (
    <div className="container">
      <div className={styles.wrapper}>
        {user ? (
          <>
            <h3>Welcome, {user.name}</h3>
            <button type="button" onClick={() => setUser(null)}>
              Logout
            </button>
          </>
        ) : (
          <>
            <button
              type="button"
              onClick={() =>
                setUser({
                  name: 'Momo',
                })
              }
            >
              Login
            </button>
          </>
        )}
      </div>
    </div>
  );
};

export default Auth;
