import { Link } from "react-router-dom";
import styles from "./404.module.scss";

const NotFound = () => {
  return (
    <div className={styles['not-found']}>
      <div className={styles.title}>404</div>
      <div className={styles.subtitle}>Page not found</div>
      <Link className={styles.link} to="/">Back to Home asd</Link>
    </div>
  );
};

export default NotFound;