import style from './Cards.module.scss';
import { CarService } from '@/services/car.service.js';
import CarItem from '@ui/CarItem/CarItem.jsx';
// import CreateCardForm from '@ui/CreateCardForm/CreateCardForm.jsx';
import Preloader from '@ui/Preloader/Preloader.jsx';
import { useQuery } from '@tanstack/react-query';

const Cards = () => {
  const { data, isLoading } = useQuery({
    queryKey: ['car'],
    queryFn: () => CarService.getAllCarsData().catch(err => console.log(err)),
  });

  console.log(isLoading);
  console.log(data);

  return (
    <div className="container">
      {/*<CreateCardForm setCars={setCarsData} />*/}
      {isLoading ? (
        <Preloader />
      ) : (
        <div className={style.wrapper}>
          {data?.length ? (
            data.map(car => <CarItem key={car.id} car={car} />)
          ) : (
            <div className={style['no-value']}>There are no cars</div>
          )}
        </div>
      )}
    </div>
  );
};

export default Cards;
