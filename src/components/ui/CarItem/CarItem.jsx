import { Link } from 'react-router-dom';
import style from './CarItem.module.scss';

const CarItem = ({ car }) => {
  const { img, name, price, id } = car;

  return (
    <div className={style.item}>
      <div className={style.card}>
        <div className={style.picture}>
          <img src={img} alt={name} loading="lazy" />
        </div>
        <div className={style.info}>
          <div className={style.name}>{name}</div>
          <div className={style.price}>
            {Intl.NumberFormat('en-US', {
              style: 'currency',
              currency: 'USD',
            }).format(price)}
          </div>
          <Link className={style['read-more']} to={`/car/${id}`}>
            Read more
          </Link>
        </div>
      </div>
    </div>
  );
};

export default CarItem;
