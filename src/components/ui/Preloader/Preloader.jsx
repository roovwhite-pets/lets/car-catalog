import preloaderSvg from './preloader.svg';
import styles from './Preloader.module.scss';

const Preloader = () => {
  return (
    <div className={ styles.preloader }>
      <img className={ styles.picture } src={ preloaderSvg } alt="" />
    </div>
  );
};

export default Preloader;