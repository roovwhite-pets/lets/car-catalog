import axios from 'axios';

export const CarService = {
  getAllCarsData: async function () {
    const url = 'https://64651d84228bd07b3542c5d2.mockapi.io/api/v1/cards';
    const responce = await axios
      .get(url)
      .catch(err => console.log('error', err));

    return responce.data;
  },

  getCarById: async function (id) {
    // eslint-disable-next-line max-len
    const url = `https://64651d84228bd07b3542c5d2.mockapi.io/api/v1/cards/${id}`;
    const responce = await axios
      .get(url)
      .catch(err => console.log('error', err));

    return responce.data;
  },
};
