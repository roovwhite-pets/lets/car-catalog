module.exports = {
  env: {
    browser: true,
    es2020: true,
    node: true
  },
  extends: [
    'eslint:recommended',
    'plugin:react/recommended',
    'plugin:react/jsx-runtime',
    'plugin:react-hooks/recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:prettier/recommended',
    'prettier',
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
  },
  plugins: [
    'react-refresh',
    '@typescript-eslint/eslint-plugin',
    '@typescript-eslint',
    'prettier',
  ],
  root: true,
  rules: {
    'react-refresh/only-export-components': 'warn',
    'semi-style': [
      'error',
      'last',
    ],
    semi: 2,
    'no-var': 2,
    indent: [
      'error',
      2
    ],
    'max-len': [
      'error',
      {
        code: 80,
        tabWidth: 2,
      }
    ],
    'object-curly-spacing': [
      'error',
      'always',
    ],
    'react/prop-types': 'off'
  },
  settings: {
    'prettier/prettier': 'error',
    react: {
      version: '18.2'
    },
  }
};
